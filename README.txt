## CrossOver project

* Ref project on Bitbucket: https://bitbucket.org/taboca/crossover-challenge-list
* New learnings: AngularJS more advanced, learned more on less.
* by Marcio dos Santos Galli
* Track log and source https://bitbucket.org/taboca/crossover-challenge-list

## Development vs Production

* npm run build - generates big files and adds the backend.js plus the angular-mocks.js
* npm run build-prod - copies the min files. See bug:PRODUCTION

### Test

* npm run dev — just for the sake of loading properly
* http://localhost:8866/index.html

## Known bugs

* Spec/PENDING — have not completed the horizontal progress bar on small blue box mainly due to time. 
* Spec/PENDING — have not completed the blank dialog mainly due to time. Prefered to have the core infrastructure and complex elements working. 
* Performance — turn off animations for the xo_item in the css
* Looks/Conformance — Find out how to display label on top of chart.
* Components —  use one graph system for both horizontal and pie. Since I have used the angular.chart.js I end up in a trap since they do not have horizontal. 
* IE<10 — flexbox workaround... is to replace
* PRODUCTION — build-prod — script needs to remove index.html s/"build-prod remove" and replace ng-app

## References

* http://compass-style.org/
* https://angularjs.org/
* https://www.npmjs.com/package/purecsso
* http://jtblin.github.io/angular-chart.js/
* https://github.com/arv/explorercanvas
* npm
* nvm

## Framework / impact

Check all the dependencies in the packages.json — you will have version information and exact information which files gets pushed to production dist. This section is here simply to add more on reason and compatibility/impact.

* npm install — to get all the libraries in the client machine / build
* On PureCSS / runtime IE8+, see references
* On http://jtblin.github.io/angular-chart.js/, which is based in Chart.js, uses canvas for IE8.
* ExCanvas for SVG / IE8ok https://github.com/arv/explorercanvas
* Angular-chart lib https://www.npmjs.com/package/angular-chart.js

## Specification

* JSON data list from server which indicates items and state/attribute information. We will have simply the JSON being caller from the client. In addition, we will have server.
* On responsiveness — we are not going with responsiveness to avoid breaking the use of the table header.
* Reference http://michalostruszka.pl/blog/2013/05/27/easy-stubbing-out-http-in-angularjs-for-backend-less-frontend-development/

## Building the project

* Install npm. We have tested with 4.2.2+
* npm install
* npm run build or npm run build-prod
