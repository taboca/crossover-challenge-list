
// This dude has an idea for optimizng the hook without the need to boostrap ng-app all the time...
myAppDev = angular.module('myAppDev', ['elementsInspectorApp', 'ngMockE2E']);
myAppDev.run(function($httpBackend) {
  // returns the current list of phones
  $httpBackend.whenGET('/update').respond(function () {
      elements[1].metrics.data.test+=1;
      return [/*status*/ 200, /*data*/ elements];
  });
});

var elements = [
  {
    'type': 'build',
    'changelist_slash_build': 'Tenrox-R1_1235',
    'owner': '',
    'time_started': '',
    'state': 'pending',
    'metrics': {
        'state':'pending',
        'data': {'test':64, 'maintainability':53, 'security':64, 'workmanship':72}
     },
    'build': {
       'state': 'pending'
       ,
       'kinds': [
         {'kind':'debug','data':'extra'},
         {'kind':'release','data':'extra'}
       ],
       'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'pending',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'pending',
      'total' : 8000,
      'covered' : 6466,
      'passed': 4321
    }
  },

  {
    'type': 'firewall',
    'changelist_slash_build': '432462',
    'owner': 'jtuck',
    'time_started': '4/18/2014 12:12pm',
    'state': 'running',
    'metrics': {
        'state':'running',
        'progress':'60%',
        'data': {'test':64, 'maintainability':53, 'security':64, 'workmanship':72}
     },
    'build': {
      'state': 'pending'
      ,
      'kinds': [
        {'kind':'debug','data':'extra'},
        {'kind':'release','data':'extra'}
      ],
      'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'pending',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'pending',
      'total' : 18786,
      'covered' : 6466,
      'passed': 4321
    }
  },

  {
    'type': 'firewall',
    'changelist_slash_build': '432461',
    'owner': 'samy',
    'time_started': '4/18/2014 10:53am',
    'state': 'rejected',
    'metrics': {
         'state':'rejected',
         'data': {'test':64, 'maintainability':-53, 'security': 64, 'workmanship':72 }
        } ,
    'build': {
       'state': 'passed'
       ,
       'kinds': [
         {'kind':'debug','data':'extra'},
         {'kind':'release','data':'extra'}
       ],
       'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'passed',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'passed',
      'total' : 18786,
      'covered' : 6466,
      'passed': 4321
    }
  },

  {
    'type': 'build',
    'changelist_slash_build': 'Tenrox-R1_1234',
    'owner': '',
    'time_started': '4/17/2014 9:42am',
    'state': 'complete',
    'metrics': {
        'state':'passed',
        'data': {'test':64, 'maintainability':53, 'security': 64, 'workmanship':72 }
    },
    'build': {
       'state': 'passed'
       ,
       'kinds': [
         {'kind':'debug','data':'extra'},
         {'kind':'release','data':'extra'}
       ],
       'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'passed',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'passed',
      'total' : 18786,
      'covered' : 6466,
      'passed': 4321
    }
  },

  {
    'type': 'firewall',
    'changelist_slash_build': '432460',
    'owner': 'samy',
    'time_started': '4/17/2014 7:51am',
    'state': 'rejected',
    'metrics': {
         'state':'rejected',
         'data': {'test':64, 'maintainability':-53, 'security': 64, 'workmanship':72 }
    },
    'build': {
       'state': 'pending'
       ,
       'kinds': [
         {'kind':'debug','data':'extra'},
         {'kind':'release','data':'extra'}
       ],
       'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'pending',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'pending',
      'total' : 18786,
      'covered' : 6466,
      'passed': 4321
    }
  },

  {
    'type': 'firewall',
    'changelist_slash_build': '432459',
    'owner': 'samy',
    'time_started': '4/16/2014 6:43am',
    'state': 'accepted',
    'metrics': {
                 'state':'passed',
                 'data': {'test':64, 'maintainability':53, 'security': 64, 'workmanship':72 }
    },
    'build': {
      'state': 'passed'
      ,
      'kinds': [
        {'kind':'debug','data':'extra'},
        {'kind':'release','data':'extra'}
      ],
      'date':'4/17/2014 10:46am'
    },
    'unit_test': {
      'state' : 'passed',
      'total' : 194,
      'covered' : 152,
      'passed' : 142
    },
    'functional_test': {
      'state' : 'passed',
      'total' : 18786,
      'covered' : 6466,
      'passed': 4321
    }
  }];
