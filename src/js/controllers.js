

var elementFolderApp = angular.module('elementsInspectorApp', ['ngAnimate','chart.js']);

elementFolderApp.controller('PanelItems',['$scope', '$http', function ($scope, $http) {

      $scope.showDetail = function (u) {
         if ($scope.activeElement != u) {
           $scope.activeElement = u;
         }
         else {
           $scope.activeElement = null;
         }
       };

       $scope.method = 'GET';
          $scope.url = '/update';

        $scope.flowInit = function() {
          $scope.code = null;
          $scope.response = null;

          $http({method: $scope.method, url: $scope.url}).
            then(function(response) {
              $scope.status = response.status;
              $scope.elements = response.data;
            }, function(response) {
              $scope.data = response.data || "Request failed";
              $scope.status = response.status;
          });
        };

        $scope.flowInit();

}]);

elementFolderApp.directive('xoElements', function() {
      return {
        restrict: 'E',
        transclude: true,
        controller: ['$scope', function($scope) {

        }],
        templateUrl: 'panel-item.html'
      };
});

elementFolderApp.directive('xoFolder', function() {
  return {
    restrict: 'E',
    templateUrl: 'panel-inspect.html'
  };
});

/* This demonstrates us modifiying JSON data which was more semantic and intented
   to beyond the textual presentation. Our JSON pending, for instance, impacts
   classes too. Therefore, to present text as "Pending" we did this little modification.
   Depending on the actual system the change could be based in a table/hash */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

elementFolderApp.controller('PanelHeader',
  ['$scope', function ($scope) {
    $scope.item.state_augumented = capitalizeFirstLetter($scope.item.state);
  }]);

elementFolderApp.controller('MetricsCtr',
  ['$scope', function ($scope) {

    if($scope.item.metrics.data.test > 0) {
        $scope.item.metrics.testarrow = "up";
    } else {
      $scope.item.metrics.testarrow = "down";
    }

    if($scope.item.metrics.data.maintainability > 0) {
        $scope.item.metrics.maintainabilityarrow = "up";
    } else {
      $scope.item.metrics.maintainabilityarrow = "down";
    }

  }]);

elementFolderApp.controller("PieCtrlUnit", function ($scope) {
        $scope.labels = ["Passed", "Failed"];
        $scope.data = [$scope.item.unit_test.passed, $scope.item.unit_test.covered-$scope.item.unit_test.passed];
        $scope.colours = [{
              fillColor: 'rgba(47, 132, 71, 0.8)',
              strokeColor: 'rgba(47, 132, 71, 0.8)',
              highlightFill: 'rgba(47, 132, 71, 0.8)',
              highlightStroke: 'rgba(47, 132, 71, 0.8)'
          },{
          fillColor: 'rgba(242, 111, 47, 0.8)',
          strokeColor: 'rgba(242, 111, 47, 0.8)',
          highlightFill: 'rgba(242, 111, 47, 0.8)',
          highlightStroke: 'rgba(242, 111, 47,  0.8)'
        }];
        $scope.options = [ { size: { width:96, height:96} } ]

        var total = parseInt($scope.item.unit_test.total);
        var passed = parseInt($scope.item.unit_test.passed);
        var covered = parseInt($scope.item.unit_test.covered);

        $scope.item.unit_test.progress = parseInt((passed/covered)*100);
        $scope.item.unit_test.progress_decimal = parseInt((passed/covered)*10);
        $scope.item.unit_test.progress_covered = parseInt((covered/total)*100);


      });

elementFolderApp.controller("PieCtrlFunctional", function ($scope) {

    $scope.labels = ["Passed", "Failed"];
    $scope.data = [$scope.item.functional_test.passed, $scope.item.functional_test.covered-$scope.item.functional_test.passed];
    $scope.colours = [{
          fillColor: 'rgba(47, 132, 71, 0.8)',
          strokeColor: 'rgba(47, 132, 71, 0.8)',
          highlightFill: 'rgba(47, 132, 71, 0.8)',
          highlightStroke: 'rgba(47, 132, 71, 0.8)'
      },{
      fillColor: 'rgba(242, 111, 47, 0.8)',
      strokeColor: 'rgba(242, 111, 47, 0.8)',
      highlightFill: 'rgba(242, 111, 47, 0.8)',
      highlightStroke: 'rgba(242, 111, 47,  0.8)'
    }];
    $scope.options = [ { size: { width:96, height:96} } ]

    var total = parseInt($scope.item.functional_test.total);
    var passed = parseInt($scope.item.functional_test.passed);
    var covered = parseInt($scope.item.functional_test.covered);

    $scope.item.functional_test.progress = parseInt((passed/covered)*100);
    $scope.item.functional_test.progress_decimal = parseInt((passed/covered)*10);
    $scope.item.functional_test.progress_covered = parseInt((covered/total)*100);

});

elementFolderApp.controller('MiniBuildCtr',
  ['$scope', function ($scope) {
}]);

elementFolderApp.controller('MiniUnitTestCtr',
  ['$scope', function ($scope) {
}]);

elementFolderApp.controller('MiniFunctionalTestCtr',
  ['$scope', function ($scope) {
}]);

elementFolderApp.controller("ResultsCtrl", function ($scope, $sce) {

    var resultState = $scope.item.state;

    if(resultState == "pending") {
        $scope.statements = {
          one: "",
          two: "Pending",
          action: ""
        }
    }

    if(resultState == "running") {
        $scope.statements = {
          one: "",
          two: "Running",
          action: ""
        }
    }

    if(resultState == "accepted") {
        $scope.statements = {
          one: "Change accepted",
          two: "Auto-merged",
          action: "See <button>Merged build</button>"
        }
        $scope.renderHTML = function () {
          return $sce.trustAsHtml($scope.statements.action);
        }
    }

    if(resultState == "rejected") {
        $scope.statements = {
          one: "Change rejected",
          two: "Metrics reduction",
          action: "<button>Find issues</button>"
        }
        $scope.renderHTML = function () {
          return $sce.trustAsHtml($scope.statements.action);
        }
    }

    if(resultState == "complete") {
        $scope.statements = {
          one: "",
          two: "Complete",
          action: "<button>Deploy</button> to <select><option>production</option></select>"
        }
        $scope.renderHTML = function () {
          return $sce.trustAsHtml($scope.statements.action);
        }
    }

});
